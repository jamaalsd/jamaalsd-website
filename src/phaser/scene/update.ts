import { setCameraZoom } from '../../lib/utils';

export default function update(this: Phaser.Scene) {
  setCameraZoom(this.cameras.main);
}
