import { phaser } from './phaser';
import { react } from './react';

export default {
  phaser,
  react,
};
