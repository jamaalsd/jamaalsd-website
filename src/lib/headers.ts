type THeaderOptions = {
  [key: string]: string
};

const headerText: THeaderOptions = {
  intro: 'Welcome to JSD.SKI',
};

export default headerText;
