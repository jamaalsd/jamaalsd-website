import yargs from 'yargs';
import Jimp from 'jimp';
import fs from 'fs';

// create an array of 0/1 values that signify if a pixel in an image is transparent or not
// using this to determine which grid tiles should appear for each setting

const argv = yargs(process.argv.slice(2)).options({
  input: { type: 'string' },
  output: { type: 'string' },
}).parseSync();

const { input, output } = argv;
if (!input || !output) {
  throw new Error('need --input and  --output');
}

Jimp.read(`${input}`)
  .then((image) => {
    const width = image.bitmap.width;
    const height = image.bitmap.height;
    const pixels = [];
    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        const p = Jimp.intToRGBA(image.getPixelColor(x, y));
        pixels.push(p.a > 0 ? 1 : 0);
      }
    }
    fs.writeFile(`${output}`, JSON.stringify({ data: pixels }), 'utf8', (err) => {
      if (err) { throw err; }
    },
    );
  })
  .catch((err) => { throw err; });
