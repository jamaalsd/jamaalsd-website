module.exports = {
  // printWidth (default: 80) - use .editorconfig
  // singleQuote (default: false) - use .editorconfig
  quoteProps: "consistent",
  trailingComma: "all",
  bracketSameLine: false,
};
